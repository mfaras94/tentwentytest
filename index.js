import React from "react";
import {createRoot} from "react-dom/client";

import App from "./src/App";
import "./public/assets/scss/main.scss";

createRoot(document.getElementById("root")).render(<App/>);
