import React from "react";
import HeroSection from "./components/HeroSection/HeroSection";
import AboutSection from "./components/AboutSection/AboutSection";



const App = () => {
  return (
    <main>
      <HeroSection />
      <AboutSection />
    
     
    </main>
  );
};

export default App;
