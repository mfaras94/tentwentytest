import React, { useState, useEffect, useRef } from "react";
import NavMenu from "../NavMenu/NavMenu";
import WelcomeText from "./WelcomeText";
import TitleText from "./TitleText";
import Thumbnail from "./Thumbnail";

const HeroSection = () => {
  const [imageUrls, setImageUrls] = useState([
    "/assets/images/bg1.avif",
    "/assets/images/bg2.avif",
    "/assets/images/bg3.avif",
    "/assets/images/bg4.avif",
  ]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const intervalIdRef = useRef(null);
  const changeBackgroundImage = () => {
    setCurrentIndex((currentIndex) => (currentIndex + 1) % imageUrls.length);
  };
  useEffect(() => {
    intervalIdRef.current = setInterval(changeBackgroundImage, 5000);
    return () => clearInterval(intervalIdRef.current);
  }, [imageUrls.length]);
  const imageUrl = `url(${imageUrls[currentIndex]})`;
  const nextImg = `url(${imageUrls[(currentIndex + 1) % imageUrls.length]})`;
  return (
    <div className="hero" style={{ backgroundImage: imageUrl }}>
      <NavMenu />
      <div className="container">
        <WelcomeText />
        <TitleText />
        <Thumbnail imageUrl={nextImg} />
      </div>
    </div>
  );
};

export default HeroSection;
