import React from "react";

const WelcomeText = () => {
  const styles = {
    color: "#F9F4EE",
  };
  return (
    <div style={styles} className="welcomeText">
      <p>Welcome To TenTwenty Farms</p>
    </div>
  );
};

export default WelcomeText;
