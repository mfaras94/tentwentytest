import React from "react";

const TitleText = (props) =>{
    const styles = {
        color: "#ffffff",
        
    }
    return(
        <div style={styles} className="titleText text-animation">
            <h1>From our</h1><h1>Farms</h1><h1>to your hands</h1>
        </div>
    )
}

export default TitleText;