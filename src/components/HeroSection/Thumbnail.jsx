import React, { useEffect, useRef } from "react";

const Thumbnail = (props, { width = 100 }) => {
  const oneRef = useRef(null);
  const twoRef = useRef(null);
  const threeRef = useRef(null);
  const fourRef = useRef(null);

  const animate = (element, property, value, className) => {
    return new Promise((resolve) => {
      if (!element.current) {
        resolve();
        return;
      }

      element.current.style[property] = `${value}% `;
      element.current.classList.add(className);

      element.current.addEventListener("animationend", () => {
        resolve();
      });
    });
  };

  const resetAnimations = () => {
    oneRef.current.style.width = "";
    twoRef.current.style.height = "";
    threeRef.current.style.width = "";
    fourRef.current.style.height = "";

    oneRef.current.classList.remove("widthAnimation");
    twoRef.current.classList.remove("heightAnimation");
    threeRef.current.classList.remove("widthAnimation");
    fourRef.current.classList.remove("heightAnimation");
    
  };

  async function runAnimations() {
    await animate(oneRef, "width", width, "widthAnimation");
    await animate(twoRef, "height", width, "heightAnimation");
    await animate(threeRef, "width", width, "widthAnimation");
    await animate(fourRef, "height", width, "heightAnimation");
    resetAnimations();
  }

  useEffect(() => {
    runAnimations();
  }, [props.imageUrl]);

  const next = () => {
    resetAnimations();
  };

  return (
    <div className="thumbnailWrap">
      <div className="one" ref={oneRef}></div>
      <div className="two" ref={twoRef}></div>
      <div className="three" ref={threeRef}></div>
      <div className="four" ref={fourRef}></div>
      <div className="thumbnailBorder">
        <div
          className="thumbImg vh-center"
          style={{ backgroundImage: props.imageUrl }}
        >
          <a href="#" onClick={next}>
            Next
          </a>
        </div>
      </div>
    </div>
  );
};

export default Thumbnail;
