import React, { useState } from "react";
import NavItem from "./NavItem";
import ContactUsBtn from "./ContactUsBtn";
import MobileMenuToggler from "./MobileMenuToggler";

const NavMenu = () => {
  // style
  const navStyles = {
    background: "#FFFFFF",
    borderBottom: "1px solid rgba(249, 244, 238, 0.2)",
  };
  const [active, setActive] = useState(false);
  const handleChange = () => {
    setActive((prevState) => !prevState);
  };

  // items array
  const navItems = ["About", "News", "Services", "Our Team", "Make Enquiry"];
  return (
    <nav style={navStyles}>
      <div className="wrapper">
        <div className={`nav-item ${active ? "active" : ""} `}>
          <ul>
            {navItems.map((item) => (
              <li key={item}>
                <NavItem text={item} />
              </li>
            ))}
          </ul>
        </div>
        <ContactUsBtn />
        <MobileMenuToggler check={handleChange} />
      </div>
    </nav>
  );
};

export default NavMenu;
