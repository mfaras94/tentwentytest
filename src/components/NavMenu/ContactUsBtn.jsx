import React from "react";

const ContactUsBtn = () => {
  const styles = {
    background: "#FFFCFA",
    border: "1px solid #000000",
  };
  return (
    <a href="#" className="nav-btn" style={styles}>
      Contact us
      <svg
        width="22"
        height="16"
        viewBox="0 0 22 16"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M20.7439 8L0.871948 8M20.7439 8L13.2919 15M20.7439 8L13.2919 1"
          stroke="#221F20"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </a>
  );
};

export default ContactUsBtn;
