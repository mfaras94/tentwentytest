import React from "react";

const MobileMenuToggler = (props) => {
  const mobileMenuToggler = {
    background: "#F9F4EE",
    border: "1px solid #F9F4EE",
  };
  return (
    <div
      style={mobileMenuToggler}
      className="vh-center menuToggle"
      onClick={props.check}
    >
      <svg
        width="22"
        height="15"
        viewBox="0 0 22 15"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <line y1="0.5" x2="22" y2="0.5" stroke="#221F20" />
        <line x1="2" y1="7.5" x2="20" y2="7.5" stroke="#221F20" />
        <line y1="14.5" x2="22" y2="14.5" stroke="#221F20" />
      </svg>
    </div>
  );
};

export default MobileMenuToggler;
