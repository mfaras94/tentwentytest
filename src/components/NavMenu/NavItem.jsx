import React from "react";

const NavItem = (props) => {
  return <a href="#">{props.text}</a>;
};

export default NavItem;
